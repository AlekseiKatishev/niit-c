#define _CRT_SECURE_NO_WARNINGS
/*
�������� ���������, ������� ��������� ������ ������������ �
����������� ���������
���������:
��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������ M;
(b) ������� ������ ������������� ������� N = 2M;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ������������ ��������;
(f) ���������� ����� ���������� ������������ ������������ � ����������� ��������;
(g) ����������� ������������ ������
*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#define RND 1000
long long size, sum;
int start, stop;
float t;
int *parr;

int take_degree() //������� �������� �� ������������ �������
{
	int degree;
	do
	{
		printf("Enter degree 1-27\n");
		scanf("%u", &degree);
	}
	while (degree < 1 || degree > 27);

	return degree;
}
int degree_calc() //������� ����������� ������� ������
{
	int i,result=1,degree;
	degree = take_degree();
	for (i = degree; i > 0; i--)
		result *= 2;
	return result;
}
void filling(int *arr)
{
	int i;
	for (i = 0; i <= size; i++)
		arr[i] = rand() % RND;
}
int trad(int *arr)
{
	int i,sum=0;
	for (i = 0; i <= size; i++)
		sum += arr[i];
	return sum;
}

long long recur(int *arr,int size)
{
	if (size == 1)
		return *arr;
	else
		return recur(arr, size / 2) + recur(arr + size / 2, size - size / 2);
}



int main()
{
	
	//a,b
	size=(degree_calc());
	//c
	if (!(parr = (int*)malloc(size * sizeof(int))))
	{
		printf("Allocation error.");
		exit(0);
	}
	//d
	srand(time(NULL));
	filling(parr);
	//e

	start = clock();
	sum = recur(parr, size);
	stop = clock();
	printf("time recursion %d tik\n", (stop - start));

	start = clock();
	sum=trad(parr);
	stop = clock();
	printf("time traditional %d tik\n", (stop - start));


	//free(parr);
	return 0;
}


