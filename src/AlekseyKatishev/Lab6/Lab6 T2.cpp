/*
�������� ���������, ������� ������� � ��������� ����� ����� ��
2 �� 1000000 �����, ����������� ����� ������� ������������������ ��������
*/
#define _CRT_SECURE_NO_WARNING
#include<stdio.h>
#define MAX 1000000

int BestResult=0,BestChoice,count;

int rec( int n)
{
	if (n > 1)
	{
		count++;
		if (n % 2 == 0)
			rec(n / 2);
		else
			rec(n * 3 + 1);
	}
	else
			return count;
}


int main()
{
	int i;
	for (i = 2; i <= MAX; i++)
	{
		rec(i);
		if (count > BestResult)
		{
			BestChoice = i;
			BestResult = count;
		}
		count = 0;
	}
	printf("Best Result = %d operation for number %d\n", BestResult, BestChoice);
	return 0;
}
