#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define SIZE 256
#include "Headrar.h"

int main(int argc, char *argv[])
{
	int i,Alphabit, tail;
	unsigned long long AllSign,cnt101;
	struct SYM arr[SIZE];
	struct SYM *parr[SIZE];
	struct SYM* root;
	FILE *fp = fopen(argv[1], "rb");
	FILE *fbin = fopen("binfile.101", "w+b");
	FILE *fcmplt = fopen("war+peace.txt.arh", "wb");
	clearArr(arr);
	AllSign=creatArr(arr, fp);
	Alphabit = knowAlph(arr);

	for (i = 0; i < Alphabit; i++)
		arr[i].freq = (float)arr[i].count / AllSign;

	exchangeArr(arr, Alphabit);

	for (i = 0; i < Alphabit; i++)
	{
		parr[i] = &arr[i];
		printf("Symbol: %c - repeated: %f\n", parr[i]->symbol, parr[i]->freq);
	}
	
	root = makeTree(parr,Alphabit-1);//������� -1 �� ������ �� 0 ���������
	root->code[0] = '1'; //������ ������ root->code = "1"; ????
	root->code[1] = '\0';
	makeCodes(root);//��������� ����
	rewind(fp);
	creatbinfile(fp, fbin, Alphabit-1, arr);//������� 101
	rewind(fbin);

	cnt101 = calc101file(fbin);//������� �-�� 0/1 � �������������
	tail = cnt101 % 8;

	
	fwrite("clear", sizeof(char), 5, fcmplt);		//�������� �������
	fwrite(&Alphabit, sizeof(char), 1, fcmplt);					//���������� ���������� ��������
	fwrite(&tail, sizeof(int), 1, fcmplt);					//�������� ������
	for (i = 0; i<Alphabit; i++)			//������� �������������
	{
		fwrite(&arr[i].symbol, sizeof(char), 1, fcmplt);
		fwrite(&arr[i].freq, sizeof(float), 1, fcmplt);
	}
	rewind(fbin);

	archivation(fbin, fcmplt, cnt101 - tail);
	archivation(fbin, fcmplt, tail);
	fclose(fp);
	fclose(fcmplt);
	return 0;
}
