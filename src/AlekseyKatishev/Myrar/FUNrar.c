#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define SIZE 256
#include "Headrar.h"

void clearArr(struct SYM *arr)
{
	int i;
	for (i = 0; i<SIZE; i++)
	{
		arr[i].symbol = 0;
		arr[i].freq = 0;
	}
}

unsigned long long  creatArr(struct SYM*arr, FILE *fp)
{
	int i;
	int buf;
	unsigned long long AllSign=0;

	while ((buf = fgetc(fp)) != EOF)
	{

		AllSign++;
		for (i = 0; i < SIZE; i++)
		{
			if (buf == arr[i].symbol)
			{
				arr[i].count++;
				break;
			}
			if (arr[i].symbol == 0)
			{
				arr[i].symbol = buf;
				arr[i].count = 1;
				arr[i].left=NULL;
				arr[i].right=NULL;
				break;
			}
		}
	}
	return AllSign;
}

int knowAlph(struct SYM *arr)
{
	int i, Alphabit = 0;
	for (i = 0; i < SIZE; i++)
	{
		if (arr[i].symbol != 0)
		{
			Alphabit++;
		}
	}
	return Alphabit;
}

void exchangeArr(struct SYM *arr,int max)
{
	int i,j;
	for (i = 1; i < max; i++)
		for (j = 0; j < max - 1; j++)
			if (arr[j].freq < arr[j + 1].freq)
			{
				struct SYM Bufer = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = Bufer;
			}
}

struct SYM* makeTree(struct SYM* root[],int cnt )
{
	struct SYM* bufer;
	struct SYM* brunch;
	brunch = ((struct SYM*)malloc(sizeof(struct SYM)));
	brunch->symbol = 0;
	brunch->freq = root[cnt]->freq + root[cnt -1]->freq;
	brunch->left = root[cnt];
	brunch->right = root[cnt - 1];

	if (cnt == 1) //����������� ����� � ������� ��������� ������� ������ 0 � 1 �����
		return brunch;
	for (int i = cnt; i >= 0; i--)
	{
		if (brunch->freq > root[i]->freq)
		{
			bufer = root[i];
			root[i] = brunch;
			if (i<cnt)
				root[i + 1]=bufer;
		}
		else
			break;
	}
	return makeTree(root, cnt - 1);
}

void makeCodes(struct SYM *root)//����������� ������� ����������
{
	if (root->left)//���� ���� ����� �����
	{
		strcpy(root->left->code, root->code);//�������� ���� ��� � �����(strcpy ��. �-� �����������)
		strcat(root->left->code, "0");//� ���������� � ���� 0 �.�. ����� �����(strcat ��. �-� ���������� �����)
		makeCodes(root->left);//������ ���� ����� ��������� ����� ����� ��� ������
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

void creatbinfile(FILE* fp, FILE* bin,int max,struct SYM mass[])//��������� ������
{
	int i;
	int buf;
	while ((buf = fgetc(fp)) != EOF)
		for (i = 0; i <= max; i++)
		{
			if(mass[i].symbol == buf)
				fputs(mass[i].code, bin);
		}
}

unsigned long long calc101file(FILE* fp)
{
	unsigned long long cnt=0;
	int buf;
	while ((buf = fgetc(fp)) != EOF)
		cnt++;
	return cnt;
}

void archivation(FILE *fp,FILE *fcmplt, int max)
{
int j = 0;
int zeroone[8] = {0};
union CODE unionBuf;
for (int i = 0; i < max; i++)
{
	zeroone[j] = fgetc(fp);
	if (j == 7)
	{
		unionBuf.byte.b1 = zeroone[0] - '0';
		unionBuf.byte.b2 = zeroone[1] - '0';
		unionBuf.byte.b3 = zeroone[2] - '0';
		unionBuf.byte.b4 = zeroone[3] - '0';
		unionBuf.byte.b5 = zeroone[4] - '0';
		unionBuf.byte.b6 = zeroone[5] - '0';
		unionBuf.byte.b7 = zeroone[6] - '0';
		unionBuf.byte.b8 = zeroone[7] - '0';
		fputc(unionBuf.sbl, fcmplt);
		j = 0;
	}
	j++;
}
}