#define _CRT_SECURE_NO_WARNING
/*
1. �������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������
*/
#define M 256
#include<stdio.h>
#include<time.h>
char result[M] = {0};
char userline[M];
char *indexes[M];

int main()
{
	srand(time(NULL));
	get_User_Line(userline);
	getWords(userline, indexes);
	mixWords(result, indexes);
	puts(result);
	return 0;
}